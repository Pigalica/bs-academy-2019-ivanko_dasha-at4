const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./specs/mytestdata.json');

    class NewList {
        static async getTokenUser(url, args) {
            const response = await fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            });
            return await response.json();
        };


        static async newListCreation(url, token, args) {
            const response = await fetch(`${url}/user-lists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                body: JSON.stringify({
                    name: args.name
                })
            });
            return await response.status;
        };


    };

    async function createNewList(url, args) {

        console.log(`Creating new list for ${url}`);

        const getAccessToken = await NewList.getTokenUser(url, args);
        const userToken = 'Bearer ' +  getAccessToken.data["access_token"];

        console.log(userToken);

        const userListCreate = await NewList.newListCreation(url, userToken, args);

        if (userListCreate === 201) {
            console.log(`New list was created`);
            return Promise.resolve();
        }

        const error = new Error(`Failed to successfully create new list`);
        error.message = '' + userListCreate;
        return Promise.reject(error);
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.response ? error.response.body : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);
    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Create New List=========');
        createNewList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}