const credentials = require('../mytestdata.json');

const listsActions = require('./mylists_pa');
const listspageSteps = new listsActions();

const mainpageActions = require('../MainPage/mainpage_pa');
const themainpageSteps = new mainpageActions();

const Assert = require('../My_helpers/ivanko_validators');

const HelpClass = require('../My_helpers/ivanko_helpers');
const help = new HelpClass();

const Wait = require('../My_helpers/ivanko_waiters');

function browserClick(el, index) {
    return browser.execute((e, i) => {document.querySelectorAll(e)[i].click();}, el, index);
 }

function waitForSpinner() {

    const spinner = $('div#preloader');
    spinner.waitForDisplayed(10000);
    spinner.waitForDisplayed(10000, true);
};

describe('Creation and deletion of lists', () => {
   
    it('Should create new list at Hedonist with valid data', () => {
        
        help.preconditions();
        browser.pause(1000);
        themainpageSteps.openDropDown();
        browserClick('a[href="/my-lists"]', 0);
        browser.pause(1000);
        browserClick('div.add-list.has-text-right > a', 0);
        listspageSteps.enternameofnewlist(credentials.name);
        listspageSteps.savenewlistbutton();
        Wait.forNotificationToAppear();
                
        Assert.textofNotificationIs(credentials.newlistSuccessCreatedText);

        help.reloading();
                    
    });
      
    it('Should be possibility to delete list at Hedonist', () => {
        
        help.preconditions();
        browser.pause(1000);
        themainpageSteps.openDropDown();
        browserClick('a[href="/my-lists"]', 0);
        waitForSpinner();
        browserClick('div.add-list.has-text-right > a', 0);
        listspageSteps.enternameofnewlist(credentials.name);
        listspageSteps.savenewlistbutton();
        listspageSteps.deletenewlistbutton();
        browser.pause(2000);
        listspageSteps.confirmtodeletenewlistbutton();
        Wait.forNotificationToAppear();

        Assert.textofNotificationIs(credentials.newlistSuccessDeletedText);

        help.reloading();
               
    });
    
});