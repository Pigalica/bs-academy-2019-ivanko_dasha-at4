const loginPage = require('./login_po');
const logpage = new loginPage();


class loginActions {

    enterEmail(value) {
        logpage.emailField.waitForDisplayed(2000);
        logpage.emailField.clearValue();
        logpage.emailField.setValue(value);
    }

    enterpassword(value) {
        logpage.passField.waitForDisplayed(2000);
        logpage.passField.clearValue();
        logpage.passField.setValue(value);
    }

    loginconfirm() {
        logpage.loginButton.waitForDisplayed(2000);
        logpage.loginButton.click();
    }

}

module.exports = loginActions;