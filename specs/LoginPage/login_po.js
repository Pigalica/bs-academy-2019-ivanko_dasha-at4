class loginPage {

    get emailField () {return $('input[name=email]')};
    get passField () {return $('input[type=password]')};
    get loginButton () {return $('button.button.is-primary')};
    
};

module.exports = loginPage;