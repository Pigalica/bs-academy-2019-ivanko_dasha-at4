const mainPage = require('./mainpage_po');
const themainpage = new mainPage();

class mainpageActions {

    openDropDown() {
        themainpage.dropDown.waitForDisplayed(5000);
        themainpage.dropDown.click();
    }
}

module.exports = mainpageActions;