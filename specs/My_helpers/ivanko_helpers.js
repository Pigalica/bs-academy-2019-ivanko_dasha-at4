const loginActions = require('../LoginPage/login_pa');
const logpageSteps = new loginActions();
const credentials = require('../mytestdata.json');

class HelpClass 
{
    
    preconditions() {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250/');
        logpageSteps.enterEmail(credentials.email);
        logpageSteps.enterpassword(credentials.password);
        logpageSteps.loginconfirm();
    }

    preconditionsfornewuser() {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250/');
    }

    reloading() {
        browser.reloadSession();
    }
    

}

module.exports = HelpClass;