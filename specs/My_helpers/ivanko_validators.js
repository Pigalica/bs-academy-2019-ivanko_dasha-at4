const assert = require('chai').assert;

class AssertHelper {

    textofNotificationIs(expectedMessage) {
        const notification = $('div.toast.is-top');
        const actualMessage = notification.getText()
        assert.equal(actualMessage, expectedMessage, `Expected ${actualMessage} to be equal to ${expectedMessage}`);
    }

}

module.exports = new AssertHelper();