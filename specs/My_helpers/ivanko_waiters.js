class CustomWaits {
    
    forNotificationToAppear() {
        const notification = $('div.toast div');
        notification.waitForDisplayed(2000);
    }

}

module.exports = new CustomWaits();