const NewPlacePage = require('../page/NewPlace_po');
const page = new NewPlacePage();

class NewPlaceActions {

    clickNextButton() {
        page.nextButton.waitForDisplayed(2000);
        page.nextButton.click();
    }

}

module.exports = NewPlaceActions;
