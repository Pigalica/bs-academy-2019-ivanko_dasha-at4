const signupPage = require('./signup_po');
const signpage = new signupPage();

class signupActions {

    openSignUpPage() {
        signpage.creatnewAccountbutton.waitForDisplayed(2000);
        signpage.creatnewAccountbutton.click();
    }
    
    enterFirstName(value) {
        signpage.first_name.waitForDisplayed(2000);
        signpage.first_name.clearValue();
        signpage.first_name.setValue(value);
    }

    enterLastName(value) {
        signpage.last_name.waitForDisplayed(2000);
        signpage.last_name.clearValue();
        signpage.last_name.setValue(value);
    }

    enterEmail(value) {
        signpage.email.waitForDisplayed(2000);
        signpage.email.clearValue();
        signpage.email.setValue(value);
    }

    enterPassword(value) {
        signpage.password.waitForDisplayed(2000);
        signpage.password.clearValue();
        signpage.password.setValue(value);
    }

    createNewUser() {
        signpage.createButton.waitForDisplayed(2000);
        signpage.createButton.click();
    }

}

module.exports = signupActions;