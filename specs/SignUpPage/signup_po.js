class signupPage {

    get creatnewAccountbutton () {return $('a.link.link-signup')};
    get first_name () {return $('input[name=firstName]')};
    get last_name () {return $('input[name=lastName]')};
    get email () {return $('input[name=email]')};
    get password () {return $('input[type=password]')};    
    get createButton () {return $('button.button.is-primary')};
    
};

module.exports = signupPage;